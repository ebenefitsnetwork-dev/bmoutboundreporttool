﻿using Newtonsoft.Json;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BMOutboundReportTool
{
    class Program
    {
        static void Main(string[] args)
        {
            logger.log.Info("Begin " + DateTime.Now.ToString());
            Console.WriteLine("Begin program ------------------");

            GetFullReportDetails();
        }
        public static void GetFullReportDetails()
        {
            try
            {
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    List<GetJobList_View> jobsList = new List<GetJobList_View>();
                    using (var database = new System.Data.Linq.DataContext(connectionString))
                    {
                        database.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                        database.CommandTimeout = 120;
                        string whereStr = @" CreationMethod = 'Scheduler' 
                                             AND JobTime is not null AND CAST(JobTime AS date) = CAST(GETDATE() AS date)
                                             AND CAST(ProcessTime AS date) = CAST(GETDATE() AS date)";
                        string query = @"select * from GetJobList_View where " + whereStr;
                        logger.log.Info("query :: " + query);
                        jobsList = database.ExecuteQuery<GetJobList_View>(query).ToList();
                    }

                    var successJobList = jobsList.Where(j => j.Status == "Succeeded" || j.Status == "Partially Succeeded").ToList();
                    var ErrorJobList = jobsList.Where(j => j.Status == "Error" || j.Status == "Stopped").ToList();
                    var PendingJobList = jobsList.Where(j => j.Status == "Pending" || j.Status == "Processing").ToList();
                    var WaitingToReviewJobList = jobsList.Where(j => j.Status == "Waiting To Review").ToList();
                    var Waiting = jobsList.Where(j => j.Status == "Waiting").ToList();
                    var Late = jobsList.Where(j => j.Status == "Late").ToList();
                    var FinishedJobList = jobsList.Where(j => j.Status == "Finished").ToList();
                    var FailedToTransmitJobList = jobsList.Where(j => j.Status == "Failed To Transmit").ToList();

                    var slDocument = new SLDocument();
                    slDocument.RenameWorksheet(slDocument.GetSheetNames()[0], "Success");
                    SuccessJobsExcelSheet(successJobList, ref slDocument);

                    slDocument.AddWorksheet("Finished");
                    SuccessJobsExcelSheet(FinishedJobList, ref slDocument);

                    slDocument.AddWorksheet("Error");
                    ErrorJobsExcelSheet(ErrorJobList, false, ref slDocument);

                    slDocument.AddWorksheet("Failed To Transmit");
                    ErrorJobsExcelSheet(FailedToTransmitJobList, true, ref slDocument);

                    slDocument.AddWorksheet("Pending or Processing");
                    PendingJobsExcelSheet(PendingJobList, ref slDocument);

                    slDocument.AddWorksheet("Waiting To Review");
                    WaitingJobsExcelSheet(WaitingToReviewJobList, ref slDocument);

                    string fileName = System.IO.Path.GetTempFileName();
                    fileName = Path.ChangeExtension(fileName, ".xlsx");
                    slDocument.SaveAs(fileName);

                    sendEmail(fileName);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null) logger.log.Error("Inner Exce=: " + ex.InnerException);
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    var line = frame.GetFileLineNumber();
                    logger.log.Error("Err mes=: " + ex.Message);
                    logger.log.Error("Stack trace=: " + ex.StackTrace);
                }

                #region old
                //using (ebnPartnerPortal_prodEntities eBND = new ebnPartnerPortal_prodEntities())
                //{
                //    var getTodaysProjects = eBND.GetOutboundReports.ToList();

                //    List<string> titles = new List<string>();
                //    titles.Add("Job Name");
                //    titles.Add("Scheduled Transmission Time");
                //    titles.Add("Status");

                //    SLDocument document = new SLDocument();
                //    Setexcelstyle(document, 3);

                //    var cellStyle = document.CreateStyle();
                //    cellStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.Black);
                //    cellStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    cellStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    cellStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    cellStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    cellStyle.Font.FontColor = System.Drawing.Color.Black;

                //    var headerStyle = document.CreateStyle();
                //    headerStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.FromArgb(59, 108, 145), System.Drawing.Color.White);
                //    headerStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    headerStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    headerStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    headerStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
                //    headerStyle.Font.FontColor = System.Drawing.Color.White;

                //    logger.log.Info("Begin fill excel document");
                //    Console.WriteLine("Begin fill excel document");

                //    var rowIndex = 1;
                //    var columnIndex = 1;
                //    foreach (var item in titles)
                //    {
                //        document.SetCellStyle(rowIndex, columnIndex, cellStyle);
                //        document.SetCellValue(rowIndex, columnIndex, item);
                //        document.SetCellStyle(rowIndex, columnIndex, headerStyle);

                //        columnIndex++;
                //    }

                //    rowIndex = 2;
                //    foreach (var job in getTodaysProjects)
                //    {
                //        document.SetCellValue(rowIndex, 1, job.JobName);
                //        document.SetCellValue(rowIndex, 2, job.JobTime.HasValue ? job.JobTime.Value.ToString() : "");
                //        document.SetCellValue(rowIndex, 3, job.Status);
                //        rowIndex++;
                //    }

                //    string fileName = System.IO.Path.GetTempFileName();
                //    fileName = Path.ChangeExtension(fileName, ".xlsx");
                //    document.SaveAs(fileName);

                //    sendEmail(fileName);
                //}
                #endregion

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.log.Error("Inner Exce=: " + ex.InnerException);
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                logger.log.Error("Err mes=: " + ex.Message);
                logger.log.Error("Stack trace=: " + ex.StackTrace);
            }
        }

        public static void Setexcelstyle(SLDocument slDocument, int colnumbers)
        {
            var cellStyle = slDocument.CreateStyle();
            cellStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.Black);
            cellStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Font.FontColor = System.Drawing.Color.Black;

            var headerStyle = slDocument.CreateStyle();
            headerStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.FromArgb(59, 108, 145), System.Drawing.Color.White);
            headerStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Font.FontColor = System.Drawing.Color.White;

            for (int i = 1; i <= colnumbers; i++)
            {
                slDocument.SetColumnWidth(i, 20);
                slDocument.SetCellStyle(1, i, headerStyle);
            }

        }

        public static void sendEmail(string fileName)
        {
            var att = new Attachment(fileName);
            att.Name = "OutboundReport_" + DateTime.Now.ToString("yyy-MM-dd") + ".xlsx";
            SndMail mail = new SndMail();
            string mailTo = ConfigurationManager.AppSettings["NotificationMails"];
            mail.Snd(mailTo, "OutboundDailyReport", "Outbound Daily Report", attachment: att);
        }

        private static void SuccessJobsExcelSheet(List<GetJobList_View> JobsList, ref SLDocument slDocument)
        {
            var cellStyle = slDocument.CreateStyle();
            cellStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.Black);
            cellStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Font.FontColor = System.Drawing.Color.Black;

            var headerStyle = slDocument.CreateStyle();
            headerStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.FromArgb(59, 108, 145), System.Drawing.Color.White);
            headerStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Font.FontColor = System.Drawing.Color.White;

            slDocument.SetColumnWidth(1, 20);
            slDocument.SetColumnWidth(2, 20);
            slDocument.SetColumnWidth(3, 20);
            slDocument.SetColumnWidth(4, 20);
            slDocument.SetColumnWidth(5, 20);
            slDocument.SetColumnWidth(6, 20);
            slDocument.SetColumnWidth(7, 20);
            slDocument.SetColumnWidth(8, 20);
            slDocument.SetColumnWidth(9, 20);
            slDocument.SetColumnWidth(10, 20);

            slDocument.SetCellStyle(1, 1, headerStyle);
            slDocument.SetCellStyle(1, 2, headerStyle);
            slDocument.SetCellStyle(1, 3, headerStyle);
            slDocument.SetCellStyle(1, 4, headerStyle);
            slDocument.SetCellStyle(1, 5, headerStyle);
            slDocument.SetCellStyle(1, 6, headerStyle);
            slDocument.SetCellStyle(1, 7, headerStyle);
            slDocument.SetCellStyle(1, 8, headerStyle);
            slDocument.SetCellStyle(1, 9, headerStyle);
            slDocument.SetCellStyle(1, 10, headerStyle);


            slDocument.SetCellValue(1, 1, "Carrier Code");
            slDocument.SetCellValue(1, 2, "Job Name");
            slDocument.SetCellValue(1, 3, "File Type");
            slDocument.SetCellValue(1, 4, "DataSet Received Date");
            slDocument.SetCellValue(1, 5, "Successful Sent Date");
            slDocument.SetCellValue(1, 6, "Status");
            slDocument.SetCellValue(1, 7, "No of Groups");
            slDocument.SetCellValue(1, 8, "Total Employees");
            slDocument.SetCellValue(1, 9, "No of Succeeded Employees");
            slDocument.SetCellValue(1, 10, "No of Failed Employees");


            string easternZoneId = "Eastern Standard Time";
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);

            var MsgIds = string.Join(",", JobsList.Select(x => x.JobMsgId?.ToString()).Distinct().ToList());
            List<SuccessModalSummary> jobsSummaryList = new List<SuccessModalSummary>();

            #region newCode
            using (var client = new HttpClient())
            {
                const int batchsize = 1000;
                var msgsList = MsgIds.Split(',').ToList();
                for (int i = 0; i < msgsList.Count(); i += batchsize)
                {
                    string url = System.Configuration.ConfigurationManager.AppSettings["BMAPI"];
                    url += "TransCars/GetJobSummary";
                    UriBuilder uriBuilder = new UriBuilder(url);

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var myObject = JsonConvert.SerializeObject(new
                    {
                        MessageIds = string.Join(",", msgsList.Skip(i).Take(batchsize).ToList())
                    });

                    client.DefaultRequestHeaders.Clear();

                    var content = new StringContent(myObject.ToString());
                    var result = client.PostAsync(url, content).Result;

                    logger.log.Info("URL :: " + uriBuilder.Uri.AbsoluteUri);

                    if (result.IsSuccessStatusCode)
                    {
                        var response = result.Content.ReadAsStringAsync().Result;

                        if (response != null)
                        {
                            jobsSummaryList.AddRange(JsonConvert.DeserializeObject<List<SuccessModalSummary>>(response));
                        }
                    }
                }
            }
            #endregion

            for (int i = 1; i <= JobsList.Count; i++)
            {
                slDocument.SetCellValue(1 + i, 1, JobsList[i - 1].CarrierCode);
                slDocument.SetCellValue(1 + i, 2, JobsList[i - 1].JobName);
                slDocument.SetCellValue(1 + i, 3, JobsList[i - 1].FileType);
                slDocument.SetCellValue(1 + i, 4, JobsList[i - 1].DataSetDate.HasValue ? JobsList[i - 1].DataSetDate.Value.ToString() : "");
                slDocument.SetCellValue(1 + i, 5, JobsList[i - 1].SuccessSentTime.HasValue ? JobsList[i - 1].SuccessSentTime.Value.ToString() : "");
                slDocument.SetCellValue(1 + i, 6, JobsList[i - 1].Status);

                string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                long ProjectId = JobsList[i - 1].ProjectId;
                Guid MessageId = JobsList[i - 1].JobMsgId.Value;



                #region newCode
                SuccessModalSummary jobSummary = jobsSummaryList.Where(x => x.MsgId == MessageId.ToString()).FirstOrDefault();
                slDocument.SetCellValue(1 + i, 7, jobSummary?.GroupsCount ?? 0);
                slDocument.SetCellValue(1 + i, 8, jobSummary?.AllEmployees ?? 0);
                slDocument.SetCellValue(1 + i, 9, (jobSummary?.SentEmployees == 0 ? jobSummary?.ValidEmployees : jobSummary?.SentEmployees) ?? 0);
                slDocument.SetCellValue(1 + i, 10, jobSummary?.InvalidEmployees ?? 0);
                #endregion

                slDocument.SetCellStyle(1 + i, 1, cellStyle);
                slDocument.SetCellStyle(1 + i, 2, cellStyle);
                slDocument.SetCellStyle(1 + i, 3, cellStyle);
                slDocument.SetCellStyle(1 + i, 4, cellStyle);
                slDocument.SetCellStyle(1 + i, 5, cellStyle);
                slDocument.SetCellStyle(1 + i, 6, cellStyle);
                slDocument.SetCellStyle(1 + i, 7, cellStyle);
                slDocument.SetCellStyle(1 + i, 8, cellStyle);
                slDocument.SetCellStyle(1 + i, 9, cellStyle);
                slDocument.SetCellStyle(1 + i, 10, cellStyle);
            }
        }

        private static void ErrorJobsExcelSheet(List<GetJobList_View> JobsList, bool FailedToTrans, ref SLDocument slDocument)
        {
            var cellStyle = slDocument.CreateStyle();
            cellStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.Black);
            cellStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Font.FontColor = System.Drawing.Color.Black;

            var headerStyle = slDocument.CreateStyle();
            headerStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.FromArgb(59, 108, 145), System.Drawing.Color.White);
            headerStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Font.FontColor = System.Drawing.Color.White;

            slDocument.SetColumnWidth(1, 20);
            slDocument.SetColumnWidth(2, 20);
            slDocument.SetColumnWidth(3, 20);
            slDocument.SetColumnWidth(4, 20);
            slDocument.SetColumnWidth(5, 20);
            slDocument.SetColumnWidth(6, 20);
            slDocument.SetColumnWidth(7, 20);

            if (!FailedToTrans)
                slDocument.SetColumnWidth(8, 20);

            slDocument.SetCellStyle(1, 1, headerStyle);
            slDocument.SetCellStyle(1, 2, headerStyle);
            slDocument.SetCellStyle(1, 3, headerStyle);
            slDocument.SetCellStyle(1, 4, headerStyle);
            slDocument.SetCellStyle(1, 5, headerStyle);
            slDocument.SetCellStyle(1, 6, headerStyle);
            slDocument.SetCellStyle(1, 7, headerStyle);

            if (!FailedToTrans)
                slDocument.SetCellStyle(1, 8, headerStyle);

            slDocument.SetCellValue(1, 1, "Carrier Code");
            slDocument.SetCellValue(1, 2, "Job Name");
            slDocument.SetCellValue(1, 3, "File Type");
            slDocument.SetCellValue(1, 4, "DataSet Received Date");
            slDocument.SetCellValue(1, 5, "Successful Sent Date");
            slDocument.SetCellValue(1, 6, "Status");
            slDocument.SetCellValue(1, 7, "Connection Type");


            if (!FailedToTrans)
                slDocument.SetCellValue(1, 8, "Error Reason");

            string easternZoneId = "Eastern Standard Time";
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);

            for (int i = 1; i <= JobsList.Count; i++)
            {
                slDocument.SetCellValue(1 + i, 1, JobsList[i - 1].CarrierCode);
                slDocument.SetCellValue(1 + i, 2, JobsList[i - 1].JobName);

                slDocument.SetCellValue(1 + i, 3, JobsList[i - 1].FileType);
                slDocument.SetCellValue(1 + i, 4, JobsList[i - 1].DataSetDate.HasValue ? JobsList[i - 1].DataSetDate.Value.ToString() : "");
                slDocument.SetCellValue(1 + i, 5, "");
                slDocument.SetCellValue(1 + i, 6, JobsList[i - 1].Status);
                slDocument.SetCellValue(1 + i, 7, !string.IsNullOrEmpty(JobsList[i - 1].CurrentPhase) && JobsList[i - 1].CurrentPhase.ToLower().Contains("production") ? "Production" : "Testing");

                if (!FailedToTrans)
                {
                    if (JobsList[i - 1].ErrorId == null)
                    {
                        slDocument.SetCellValue(1 + i, 8, "There is an internal error, please refer to eBN team");
                    }
                    else
                    {
                        string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                        using (var database = new System.Data.Linq.DataContext(connectionString))
                        {
                            if (JobsList[i - 1].ErrorTableName == "SageMessage")
                            {
                                var filePath = database.ExecuteQuery<string>(@"SELECT TOP 1 M.ErrorPath
                                                                            FROM SageServer.dbo.SageMessage M
                                                                            WHERE M.Id = {0}", JobsList[i - 1].ErrorId).FirstOrDefault();
                                string content = "";
                                try
                                {
                                    string[] lines = System.IO.File.ReadAllLines(filePath);
                                    foreach (string line in lines)
                                    {
                                        content += line;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (ex.InnerException != null) logger.log.Error("Inner Exce=: " + ex.InnerException);
                                    logger.log.Error("Err mes=: " + ex.Message);
                                    logger.log.Error("Stack trace=: " + ex.StackTrace);
                                }

                                slDocument.SetCellValue(1 + i, 8, content);
                            }
                            else
                            {
                                var ErrorMsg = database.ExecuteQuery<string>(@"SELECT TOP 1 M.Message
                                                                            FROM SageServer.dbo.DataCollectorLog M
                                                                            WHERE M.Id = {0}", JobsList[i - 1].ErrorId).FirstOrDefault();

                                slDocument.SetCellValue(1 + i, 8, ErrorMsg);
                            }
                        }
                    }
                }

                slDocument.SetCellStyle(1 + i, 1, cellStyle);
                slDocument.SetCellStyle(1 + i, 2, cellStyle);
                slDocument.SetCellStyle(1 + i, 3, cellStyle);
                slDocument.SetCellStyle(1 + i, 4, cellStyle);
                slDocument.SetCellStyle(1 + i, 5, cellStyle);
                slDocument.SetCellStyle(1 + i, 6, cellStyle);
                slDocument.SetCellStyle(1 + i, 7, cellStyle);

                if (!FailedToTrans)
                    slDocument.SetCellStyle(1 + i, 8, cellStyle);
            }
        }

        private static void PendingJobsExcelSheet(List<GetJobList_View> JobsList, ref SLDocument slDocument)
        {
            var cellStyle = slDocument.CreateStyle();
            cellStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.Black);
            cellStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Font.FontColor = System.Drawing.Color.Black;

            var headerStyle = slDocument.CreateStyle();
            headerStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.FromArgb(59, 108, 145), System.Drawing.Color.White);
            headerStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Font.FontColor = System.Drawing.Color.White;

            slDocument.SetColumnWidth(1, 20);
            slDocument.SetColumnWidth(2, 20);
            slDocument.SetColumnWidth(3, 20);
            slDocument.SetColumnWidth(4, 20);
            slDocument.SetColumnWidth(5, 20);
            slDocument.SetColumnWidth(6, 20);
            slDocument.SetColumnWidth(7, 20);
            slDocument.SetColumnWidth(8, 20);

            slDocument.SetCellStyle(1, 1, headerStyle);
            slDocument.SetCellStyle(1, 2, headerStyle);
            slDocument.SetCellStyle(1, 3, headerStyle);
            slDocument.SetCellStyle(1, 4, headerStyle);
            slDocument.SetCellStyle(1, 5, headerStyle);
            slDocument.SetCellStyle(1, 6, headerStyle);
            slDocument.SetCellStyle(1, 7, headerStyle);
            slDocument.SetCellStyle(1, 8, headerStyle);

            slDocument.SetCellValue(1, 1, "Carrier Code");
            slDocument.SetCellValue(1, 2, "Job Name");
            slDocument.SetCellValue(1, 3, "File Type");
            slDocument.SetCellValue(1, 4, "DataSet Received Date");
            slDocument.SetCellValue(1, 5, "Successful Sent Date");
            slDocument.SetCellValue(1, 6, "Status");
            slDocument.SetCellValue(1, 7, "Execution Start Date");
            slDocument.SetCellValue(1, 8, "Connection Type");


            string easternZoneId = "Eastern Standard Time";
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);

            for (int i = 1; i <= JobsList.Count; i++)
            {
                slDocument.SetCellValue(1 + i, 1, JobsList[i - 1].CarrierCode);
                slDocument.SetCellValue(1 + i, 2, JobsList[i - 1].JobName);
                slDocument.SetCellValue(1 + i, 3, JobsList[i - 1].FileType);
                slDocument.SetCellValue(1 + i, 4, JobsList[i - 1].DataSetDate.HasValue ? JobsList[i - 1].DataSetDate.Value.ToString() : "");
                slDocument.SetCellValue(1 + i, 5, "");
                slDocument.SetCellValue(1 + i, 6, JobsList[i - 1].Status);
                slDocument.SetCellValue(1 + i, 7, JobsList[i - 1].JobStartTime.HasValue ? JobsList[i - 1].JobStartTime.Value.ToString() : "");
                slDocument.SetCellValue(1 + i, 8, JobsList[i - 1].CurrentPhase.ToLower().Contains("production") ? "Production" : "Testing");


                slDocument.SetCellStyle(1 + i, 1, cellStyle);
                slDocument.SetCellStyle(1 + i, 2, cellStyle);
                slDocument.SetCellStyle(1 + i, 3, cellStyle);
                slDocument.SetCellStyle(1 + i, 4, cellStyle);
                slDocument.SetCellStyle(1 + i, 5, cellStyle);
                slDocument.SetCellStyle(1 + i, 6, cellStyle);
                slDocument.SetCellStyle(1 + i, 7, cellStyle);
                slDocument.SetCellStyle(1 + i, 8, cellStyle);
            }
        }

        private static void WaitingJobsExcelSheet(List<GetJobList_View> JobsList, ref SLDocument slDocument)
        {
            var cellStyle = slDocument.CreateStyle();
            cellStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.Black);
            cellStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            cellStyle.Font.FontColor = System.Drawing.Color.Black;

            var headerStyle = slDocument.CreateStyle();
            headerStyle.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.FromArgb(59, 108, 145), System.Drawing.Color.White);
            headerStyle.Border.SetBottomBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetTopBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetRightBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Border.SetLeftBorder(DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin, SLThemeColorIndexValues.Dark1Color);
            headerStyle.Font.FontColor = System.Drawing.Color.White;

            slDocument.SetColumnWidth(1, 20);
            slDocument.SetColumnWidth(2, 20);
            slDocument.SetColumnWidth(3, 20);
            slDocument.SetColumnWidth(4, 20);
            slDocument.SetColumnWidth(5, 20);
            slDocument.SetColumnWidth(6, 20);
            slDocument.SetColumnWidth(7, 20);

            slDocument.SetCellStyle(1, 1, headerStyle);
            slDocument.SetCellStyle(1, 2, headerStyle);
            slDocument.SetCellStyle(1, 3, headerStyle);
            slDocument.SetCellStyle(1, 4, headerStyle);
            slDocument.SetCellStyle(1, 5, headerStyle);
            slDocument.SetCellStyle(1, 6, headerStyle);
            slDocument.SetCellStyle(1, 7, headerStyle);

            slDocument.SetCellValue(1, 1, "Carrier Code");
            slDocument.SetCellValue(1, 2, "Job Name");
            slDocument.SetCellValue(1, 3, "File Type");
            slDocument.SetCellValue(1, 4, "DataSet Received Date");
            slDocument.SetCellValue(1, 5, "Successful Sent Date");
            slDocument.SetCellValue(1, 6, "Status");
            slDocument.SetCellValue(1, 7, "Connection Type");


            string easternZoneId = "Eastern Standard Time";
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);

            for (int i = 1; i <= JobsList.Count; i++)
            {
                slDocument.SetCellValue(1 + i, 1, JobsList[i - 1].CarrierCode);
                slDocument.SetCellValue(1 + i, 2, JobsList[i - 1].JobName);
                slDocument.SetCellValue(1 + i, 3, JobsList[i - 1].FileType);
                slDocument.SetCellValue(1 + i, 4, JobsList[i - 1].DataSetDate.HasValue ? JobsList[i - 1].DataSetDate.Value.ToString() : "");
                slDocument.SetCellValue(1 + i, 5, "");
                slDocument.SetCellValue(1 + i, 6, JobsList[i - 1].Status);
                slDocument.SetCellValue(1 + i, 7, JobsList[i - 1].CurrentPhase.ToLower().Contains("production") ? "Production" : "Testing");


                slDocument.SetCellStyle(1 + i, 1, cellStyle);
                slDocument.SetCellStyle(1 + i, 2, cellStyle);
                slDocument.SetCellStyle(1 + i, 3, cellStyle);
                slDocument.SetCellStyle(1 + i, 4, cellStyle);
                slDocument.SetCellStyle(1 + i, 5, cellStyle);
                slDocument.SetCellStyle(1 + i, 6, cellStyle);
                slDocument.SetCellStyle(1 + i, 7, cellStyle);
            }
        }

    }
}
