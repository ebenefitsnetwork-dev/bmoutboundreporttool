﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BMOutboundReportTool
{
    public class SndMail
    {
        private static SmtpSection smtpSection; // ebnnotification
        private static MailMessage Msg;
        private static SmtpClient SmtpClnt;

        public bool Snd(string EmailTo = null, string MailType = null, string Subject = null, Attachment attachment = null, List<string> CC = null)
        {
            try
            {
                smtpSection = (SmtpSection)System.Configuration.ConfigurationManager.GetSection("mailSettings/smtp");
                using (SmtpClnt = new SmtpClient(smtpSection.Network.Host, smtpSection.Network.Port))
                {
                    SmtpClnt.Credentials = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
                    SmtpClnt.EnableSsl = true;

                    using (Msg = new MailMessage())
                    {
                        Msg.From = new MailAddress(smtpSection.Network.UserName, smtpSection.Network.UserName);
                        string[] mailsTo = EmailTo.Split(';');

                        foreach (var mail in mailsTo)
                        {
                            if (mail == "")
                                break;
                            Msg.To.Add(new MailAddress(mail, mail));
                        }

                        if (Subject != null)
                            Msg.Subject = Subject;
                        if (CC != null)
                        {
                            foreach (var cc in CC)
                            {
                                Msg.CC.Add(new MailAddress(cc));
                            }
                        }


                        Msg.IsBodyHtml = true;

                        string path = null;
                        string mailBody = null;

                        path = Path.Combine(Path.GetDirectoryName(typeof(SndMail).Assembly.Location), "Content", "OutboundDailyReport.html");
                        mailBody = (System.IO.File.ReadAllText(path));

                        Msg.Body = mailBody;
                        Msg.Attachments.Add(attachment);
                        SmtpClnt.Send(Msg);

                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.log.Info("Inner Exce=: " + ex.InnerException);
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                logger.log.Info("Err mes=: " + ex.Message);
                logger.log.Info("line=: " + line);

                return false;
            }

        }

        public void Dispose()
        {
            // system will remove objects from garbage collector when system don't use it for a long time  
            GC.SuppressFinalize(this);

        }
    }
}
