﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMOutboundReportTool
{
    public class SuccessModalSummary
    {
        public int AllEmployees { get; set; }
        public int SentEmployees { get; set; }
        public int ValidEmployees { get; set; }
        public int InvalidEmployees { get; set; }
        public int SkippedEmployees { get; set; }
        public int GroupsCount { get; set; }
        public string ProjectIds { get; set; }
        public string MsgId { get; set; }
    }
}
